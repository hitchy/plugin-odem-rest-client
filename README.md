# @hitchy/plugin-odem-rest-client

_a client-side implementation for conveniently accessing [Hitchy's REST API](https://www.npmjs.com/package/hitchy-plugin-odem-rest)_

## License

[MIT](LICENSE)

## Usage

This package is providing code for integrating with a client-side application. It is designed to work with any available framework based on Javascript. Thus, there is little information on how to particularly integrate it with your application.

It all starts with installing the package as a dependency of your application's project:

```bash
npm install @hitchy/plugin-odem-rest-client
```

Next it is up to you to use it in context of your application as usual.

```javascript
import { Model } from "@hitchy/plugin-odem-rest-client";

Model.getModels()
    .then( models => {
        const { Book } = models;

        const book = new Book();
        book.title = "A Fancy Tale of Fairies";
        book.publishedIn = 2014;

        return book.save()
            .then( savedBook => {
                console.log( `book has been assigned UUID ${savedBook.uuid}` );
        
                return new Book( savedBook.uuid ).load();
            } )
            .then( loadedBook => {
                console.log( book.title === loadedBook.title ); // -> true

                return Book.find( 
                    { gte: { name: "publishedIn", value: 2010 } },
                    { limit: 10, offset: 5 }
                );
            } )
            .then( ( { items: booksSince2010, count: total } ) => {
                console.log( `total number of matches: ${total}` );
                console.log( `excerpt of at most 10 matches`, booksSince2010 );
            } );
    } );
```

The provided API is **a subset** of [Model found in server-side ODM of Hitchy](https://odem.hitchy.org/api/model.html). In fact, this package is _imitating_ that API to some extent to simplify its adoption when frequently working on either side.

Noteworthy commonalities are:

* The constructor accepts UUID on known items in preparation for loading them afterwards.
* Properties are exposed for generic read/write access.
* Instance methods `load()`, `save()` and `remove()` are available.
* Class methods `list()`, `find()` and `findByAttribute()` are basically supported.
* Property types and related constraints are basically obeyed on validation and normalization.

In some aspects the client-side implementation is significantly different from server-side API:

* There is no `$properties` for explicitly exposing all supported properties in a safe container. Thus, tracking changed properties is limited, either.
* The constructor accepts external objects provided for tracking item's properties and errors per item's property. This is available to inject reactive objects in either case for integrating with frameworks like [Vue.js](https://vuejs.org).
* Indices are found on server-side, only
* There is no separate method for validating properties. Instead, either property's value is instantly validated and normalized on assignment. Validation results are instantly exposed in properties `$valid` and `$errors`.

  On validation issues special exceptions are thrown instantly providing some type of validation issue as well as a short detail on what went wrong.
* There are no life cycle hooks.
* There is no support for defining a model. Models are defined on server-side, only, by intention.
* Supported class methods have limited support for customizing retrieval of matches.
* Schema exposed per model is limited to the model's name, properties and names of computed properties. For security reasons, no server-side code is exposed.
