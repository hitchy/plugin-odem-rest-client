/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

/* eslint-disable max-len */
/**
 * @typedef {object} UnaryFilter
 * @property {string} name name of property to test
 *
 * @typedef {object} BinaryFilter
 * @property {string} name name of property to test
 * @property {*} value value of property to compare values of selected property with
 *
 * @typedef {object} RangeFilter
 * @property {string} name name of property to test
 * @property {*} lower lower limit on values of property to accept
 * @property {*} upper upper limit on values of property to accept
 *
 * @typedef {object} NullFilter
 * @property {UnaryFilter} null requests to accept records with property value unset
 *
 * @typedef {object} NotNullFilter
 * @property {UnaryFilter} notnull requests to accept records with property value basically set
 *
 * @typedef {object} EqualityFilter
 * @property {BinaryFilter} eq requests to accept values equal provided one
 *
 * @typedef {object} InequalityFilter
 * @property {BinaryFilter} neq requests to accept values not equal provided one
 *
 * @typedef {object} LessThanFilter
 * @property {BinaryFilter} lt requests to accept values less than provided one
 *
 * @typedef {object} LessThanOrEqualFilter
 * @property {BinaryFilter} lte requests to accept values less than or equal provided one
 *
 * @typedef {object} GreaterThanFilter
 * @property {BinaryFilter} gt requests to accept values greater than provided one
 *
 * @typedef {object} GreaterThanOrEqualFilter
 * @property {BinaryFilter} gte requests to accept values greater than or equal provided one
 *
 * @typedef {object} BetweenFilter
 * @property {RangeFilter} between requests to accept values in provided range
 *
 * @typedef {(NullFilter|NotNullFilter|EqualityFilter|InequalityFilter|LessThanFilter|LessThanOrEqualFilter|GreaterThanFilter|GreaterThanOrEqualFilter|BetweenFilter)} Filter
 */
/* eslint-enable max-len */

export { Model } from "./lib/model";
