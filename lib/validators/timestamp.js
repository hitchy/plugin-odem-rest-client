/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { ISSUE_RANGE, ISSUE_REQUIRED, ValidationError } from "../error";

/**
 * Implements type-specific code for validating and normalizing values of
 * date/time properties.
 */
export class TimestampValidator {
	/**
	 * Validates and normalizes provided value to comply with definition of
	 * date/time property.
	 *
	 * @param {ModelPropertyTimestampDefinition} definition definition of date/time-type property
	 * @param {*} value value to be validated and normalized
	 * @returns {?Date} normalized value
	 */
	static validate( definition, value ) {
		if ( ( value == null || value === "" ) && definition.required && definition.default == null ) {
			throw new ValidationError( ISSUE_REQUIRED, "value is required" );
		}

		const _value = value == null ? definition.default == null ? null : new Date( definition.default ) : new Date( value );

		if ( _value == null ) {
			return null;
		}

		if ( definition.time === false ) {
			_value.setHours( 0, 0, 0, 0 );
		}

		if ( definition.min != null ) {
			const min = new Date( definition.min );

			if ( _value.getTime() < min.getTime() ) {
				throw new ValidationError( ISSUE_RANGE, "timestamp is too early" );
			}
		}

		if ( definition.max != null ) {
			const max = new Date( definition.max );

			if ( _value.getTime() > max.getTime() ) {
				throw new ValidationError( ISSUE_RANGE, "timestamp is too late" );
			}
		}

		if ( definition.step > 0 ) {
			const ref = ( definition.min == null ? new Date( "1970-01-01T00:00:00" ) : new Date( definition.min ) ).getTime();
			const step = parseInt( definition.step );

			_value.setTime( ( Math.round( ( _value.getTime() - ref ) / step ) * step ) + ref );
		}

		return _value;
	}
}
