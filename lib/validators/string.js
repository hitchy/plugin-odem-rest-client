/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { ISSUE_LENGTH, ISSUE_PATTERN, ISSUE_REQUIRED, ValidationError } from "../error";

/**
 * Implements type-specific code for validating and normalizing values of string
 * properties.
 */
export class StringValidator {
	/**
	 * Validates and normalizes provided value to comply with definition of
	 * string property.
	 *
	 * @param {ModelPropertyStringDefinition} definition definition of string-type property
	 * @param {*} value value to be validated and normalized
	 * @returns {?string} normalized value
	 */
	static validate( definition, value ) {
		if ( ( value == null || value === "" ) && definition.required && definition.default == null ) {
			throw new ValidationError( ISSUE_REQUIRED, "value is required" );
		}

		let _value = value == null ? definition.default == null ? null : String( definition.default ) : String( value );

		if ( _value == null ) {
			return null;
		}

		if ( definition.trim ) {
			_value = _value.trim();
		}

		if ( definition.reduceSpace ) {
			_value = _value.replace( /\s+/g, " " );
		}

		if ( definition.upperCase ) {
			_value = _value.toUpperCase();
		}

		if ( definition.lowerCase ) {
			_value = _value.toLowerCase();
		}

		if ( definition.minLength > 0 && _value.length < definition.minLength ) {
			throw new ValidationError( ISSUE_LENGTH, "value is too short" );
		}

		if ( definition.maxLength > 0 && _value.length > definition.maxLength ) {
			throw new ValidationError( ISSUE_LENGTH, "value is too long" );
		}

		if ( definition.pattern && !new RegExp( definition.pattern ).test( _value ) ) {
			throw new ValidationError( ISSUE_PATTERN, "value does not comply with pattern" );
		}

		return _value;
	}
}
