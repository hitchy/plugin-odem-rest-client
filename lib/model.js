/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { OptionsListCache } from "./options-list-cache";
import { ValidationError, ISSUE_UNKNOWN } from "./error";
import { PtnFormattedUuid } from "./basics";
import { StringValidator } from "./validators/string";
import { NumericValidator } from "./validators/numeric";
import { IntegerValidator } from "./validators/integer";
import { BooleanValidator } from "./validators/boolean";
import { TimestampValidator } from "./validators/timestamp";
import { ReferenceValidator } from "./validators/reference";

let BackendBaseUrl = "/api/";

let $$schemata = null;
let $$models = null;

let cachedModels = null;

/**
 * Implements common behaviour of client side models.
 *
 * @note This implementation is partially adopting API of server-side
 *       hitchy-plugin-odem.
 */
export class Model {
	/**
	 * @param {string} uuid UUID of item
	 * @param {object<string,*>} properties record of item's properties
	 * @param {object<string,(Error|string)>} errors externally provided container of per-property errors
	 */
	constructor( uuid = null, properties = {}, errors = {} ) {
		const schema = ( this.constructor.$$cachedSchemata || {} )[this.constructor.slug];
		if ( !schema || !schema.props ) {
			throw new TypeError( `missing schema of ${this.constructor.modelName} for constructing instance` );
		}

		const changed = {};
		const _errors = errors || {};

		Object.defineProperty( this, "uuid", uuid == null ? {
			value: null,
			enumerable: true,
			configurable: true,
		} : {
			value: this.constructor.normalizeUuid( uuid ),
			enumerable: true,
		} );

		const propNames = Object.keys( schema.props );
		const numProps = propNames.length;
		const that = this;

		Object.defineProperties( this, {
			$changed: { get: () => Object.keys( changed ) },
			$exists: { get: () => Boolean( this.uuid ) },
			$errors: { value: _errors },
			$valid: { get: () => !propNames.some( name => _errors[name] ) },
		} );

		for ( let i = 0; i < numProps; i++ ) {
			( function( propName ) {
				const normalizer = that.constructor.selectNormalizer( schema.props[propName] );
				let original = null;

				try {
					const value = normalizer( properties[propName] );
					if ( that.uuid ) {
						original = value;
					}
					_errors[propName] = null;
				} catch ( error ) {
					_errors[propName] = error instanceof ValidationError ? error : new ValidationError( ISSUE_UNKNOWN, "validation failed" );
				}

				Object.defineProperty( that, propName, {
					get: () => ( changed.hasOwnProperty( propName ) ? properties[propName] : original ),
					set: value => {
						try {
							properties[propName] = normalizer( value );
							_errors[propName] = null;
						} catch ( error ) {
							properties[propName] = value;
							_errors[propName] = error instanceof ValidationError ? error : new ValidationError( ISSUE_UNKNOWN, "validation failed" );
						}

						changed[propName] = properties[propName] !== original;
					},
					enumerable: true,
				} );
			} )( propNames[i] );
		}
	}

	/**
	 * Retrieves set of listeners per event associated with current model.
	 *
	 * @return {Map<string, Array<Function>>} maps event names into listener callbacks to invoke
	 * @protected
	 */
	static get listeners() {
		if ( !this.$$listeners ) {
			Object.defineProperty( this, "$$listeners", { value: new Map() } );
		}

		return this.$$listeners;
	}

	/**
	 * Registers listener to be invoked whenever emitting named event in context
	 * of current model.
	 *
	 * @param {string} name name of event to remove handler(s) from, omit for removing any event
	 * @param {function} handler handler to remove
	 * @returns {class<Model>} current model's implementation, fluent interface
	 */
	static on( name, handler ) {
		if ( typeof name === "string" && name.trim().length > 0 && typeof handler === "function" ) {
			let list = this.listeners.get( name );

			if ( !Array.isArray( list ) ) {
				list = [];
				this.listeners.set( name, list );
			}

			const index = list.indexOf( handler );

			if ( index < 0 ) {
				list.push( handler );
			}
		}

		return this;
	}

	/**
	 * Registers provided callback to be invoked once when named event is
	 * emitted the next time.
	 *
	 * @param {string} name name of event
	 * @param {function} handler callback to invoke next time event is emitted.
	 * @returns {class<Model>} current model's implementation, fluent interface
	 */
	static once( name, handler ) {
		const that = this;

		return this.on( name, _once );

		/**
		 * Invokes provided handler and instantly removes it as listener from
		 * selected event.
		 *
		 * @param {*} args parameters to emitted event
		 * @returns {void}
		 * @private
		 */
		function _once( ...args ) {
			that.off( name, _once );

			handler( ...args );
		}
	}

	/**
	 * Removes listener from model-related event.
	 *
	 * @param {string} name name of event to remove handler(s) from, omit for removing any event
	 * @param {function} handler handler to remove
	 * @returns {class<Model>} current model's implementation, fluent interface
	 */
	static off( name = null, handler = null ) {
		if ( name == null ) {
			this.listeners.clear();
		} else if ( handler == null ) {
			this.listeners.delete( name );
		} else {
			const list = this.listeners.get( name );

			if ( Array.isArray( list ) ) {
				const index = list.indexOf( handler );

				if ( index > -1 ) {
					list.splice( index, 1 );
				}
			}
		}

		return this;
	}

	/**
	 * Emits named event with attached parameters in context of current model.
	 *
	 * @param {string} name name of event to emit
	 * @param {*} args set of parameters to pass into either invoked event handler
	 * @returns {class<Model>} fluent interface
	 */
	static emit( name, ...args ) {
		const list = this.listeners.get( name );

		if ( Array.isArray( list ) ) {
			const numHandlers = list.length;

			for ( let i = 0; i < numHandlers; i++ ) {
				try {
					list[i]( ...args );
				} catch ( error ) {
					console.error( `handler for Model-event "${name}" failed:`, error );
				}
			}
		}

		return this;
	}

	/**
	 * Tests if provided value is a Model instance.
	 *
	 * @param {*} value value to test
	 * @returns {boolean} true if value is an instance of a Model
	 */
	static isModel( value ) {
		return value && value instanceof Model;
	}

	/**
	 * Adjusts base URL of currently used backend service.
	 *
	 * @param {string} baseUrl new base URL
	 * @returns {void}
	 */
	static setBackendBaseUrl( baseUrl ) {
		BackendBaseUrl = baseUrl.replace( /\/+$/, "" ) + "/";

		$$schemata = null;
		$$models = null;
	}

	/**
	 * Converts kebab-case slug into PascalCase name.
	 *
	 * @param {string} slug slug to convert
	 * @returns {string} name related to provided slug
	 */
	static slugToName( slug ) {
		return slug.replace( /(?:^|-)([a-z])/, ( _, letter ) => letter.toUpperCase() );
	}

	/**
	 * Converts PascalCase name into kebab-case slug.
	 *
	 * @param {string} name name to convert
	 * @returns {string} slug related to provided name
	 */
	static nameToSlug( name ) {
		return name.replace( /[A-Z]/, "-$0" ).replace( /^-/, "" ).toLowerCase();
	}

	/**
	 * Fetches implementation of selected model.
	 *
	 * @param {string} className name of class implementing desired model
	 * @returns {class<Model>} cached model's implementation
	 * @throws if selected model does not exist or cache hasn't been populated before
	 */
	static tryModel( className ) {
		if ( cachedModels && cachedModels.hasOwnProperty( className ) ) {
			return cachedModels[className];
		}

		throw new Error( `invalid request for missing model ${className}` );
	}

	/**
	 * Fetches set of known models.
	 *
	 * @param {boolean} forceUpdate set true to force fetching updated schemata from backend service
	 * @returns {Promise<object<string, class<Model>>>} promises map of model classes
	 */
	static getModels( forceUpdate = false ) {
		if ( forceUpdate || !$$models ) {
			$$models = this.schemata( forceUpdate )
				.then( schemata => {
					const slugs = Object.keys( schemata );
					const numSlugs = slugs.length;
					const classes = {};

					for ( let i = 0; i < numSlugs; i++ ) {
						const slug = slugs[i];
						const schema = schemata[slug];
						const name = schema.name || slug.replace( /(?:^|-)([a-z])/g, ( _, letter ) => letter.toUpperCase() );

						if ( /^[a-z][a-z0-9]+$/i.test( name ) ) {
							classes[name] = new Function( "BaseModel", "schema", `return class ${name} extends BaseModel {
								static get modelName() { return "${name}"; }
								static get slug() { return "${slug}"; }
								static get schema() { return schema; }
							}` )( Model, schema );
						}
					}

					cachedModels = classes;

					return classes;
				} );
		}

		return $$models;
	}

	/**
	 * Normalizes and validates provided value for containing UUID.
	 *
	 * @param {*} raw value to be normalized
	 * @returns {string} normalized UUID
	 */
	static normalizeUuid( raw ) {
		const prepared = String( raw ).trim();

		if ( PtnFormattedUuid.test( prepared ) ) {
			return prepared.toLowerCase();
		}

		throw new TypeError( `invalid UUID: ${raw}` );
	}

	/**
	 * Generates function for validating and normalizing values according to
	 * provided definition of property.
	 *
	 * @param {object} propertyDefinition definition of property in schema
	 * @returns {*} normalized value of property
	 * @throws ValidationError on validation issues
	 */
	static selectNormalizer( propertyDefinition ) {
		switch ( String( propertyDefinition.type || "string" ).toLowerCase() ) {
			case "string" :
				return StringValidator.validate.bind( StringValidator, propertyDefinition );

			case "number" :
			case "numeric" :
			case "decimal" :
			case "float" :
				return NumericValidator.validate.bind( NumericValidator, propertyDefinition );

			case "integer" :
				return IntegerValidator.validate.bind( IntegerValidator, propertyDefinition );

			case "boolean" :
				return BooleanValidator.validate.bind( BooleanValidator, propertyDefinition );

			case "date" :
			case "time" :
				return TimestampValidator.validate.bind( TimestampValidator, propertyDefinition );

			case "uuid" :
				return ReferenceValidator.validate.bind( ReferenceValidator, propertyDefinition );

			default :
				return value => value;
		}
	}

	/**
	 * Queries backend service over HTTP(S) connection.
	 *
	 * @param {string} method HTTP method to use
	 * @param {string|string[]} url URL to be appended to base URL of backend service, may be list of segments to be compiled into path
	 * @param {object|string|Blob|ReadableStream|FormData} body body to be sent
	 * @param {object} query query parameters to inject into URL
	 * @param {object} headers set of request headers to send
	 * @returns {Promise<*>} response with data returned in response body
	 */
	static query( method, url, body = null, query = {}, headers = {} ) {
		const options = {
			method: method || "GET",
			headers: Object.assign( {}, headers ),
		};

		let type = null;

		if ( body != null ) {
			if ( body instanceof Blob || body instanceof ReadableStream ) {
				type = "application/octet-stream";
				options.body = body;
			} else if ( body instanceof FormData ) {
				type = "application/x-www-form-urlencoded; charset=utf8";
				options.body = body;
			} else if ( typeof body === "object" ) {
				type = "application/json; charset=utf8";
				options.body = JSON.stringify( body );
			} else if ( typeof body === "string" ) {
				type = "text/plain; charset=utf8";
				options.body = body;
			}
		}

		let _url = Array.isArray( url ) ? url.join( "/" ) : url;

		if ( url.indexOf( "?" ) < 0 ) {
			const names = Object.keys( query || {} );
			const numNames = names.length;

			if ( numNames > 0 ) {
				const params = new Array( numNames );

				for ( let i = 0; i < numNames; i++ ) {
					const name = names[i];
					const value = query[name];

					if ( typeof value === "string" && value ) {
						params[encodeURIComponent( name )] = encodeURIComponent( JSON.stringify( value ) );
					} else {
						params[encodeURIComponent( name )] = encodeURIComponent( String( value ) );
					}
				}

				_url += "?" + params.join( "&" );
			}
		}

		const request = new Request( BackendBaseUrl + _url, options );

		if ( type && !Object.keys( options.headers ).some( key => key.toLowerCase() === "content-type" ) ) {
			request.headers.set( "Content-Type", type );
		}

		return fetch( request )
			.then( response => {
				if ( !response.ok ) {
					if ( response.headers.get( "Content-Type" ).match( /\/json\s*(;|$)/ ) ) {
						return response.json()
							.then( info => {
								if ( info && info.error ) {
									throw new Error( info.error );
								}

								throw new Error( `backend responded on error: ${response.status} - ${response.statusText}` );
							} );
					}

					throw new Error( `backend responded on error: ${response.status} - ${response.statusText}` );
				}

				return response.json();
			} );
	}

	/**
	 * Fetches name of current model.
	 *
	 * @returns {string} name of current model
	 */
	static get modelName() {
		return this.name;
	}

	/**
	 * Fetches slug of current model for use with addressing its related
	 * endpoints in REST API.
	 *
	 * @returns {string} slug of current model
	 */
	static get slug() {
		const pattern = /([A-Z])/g;
		pattern.lastIndex = 1;

		return this.modelName.replace( pattern, "-$1" ).replace( /^-/, "" ).toLowerCase();
	}

	/**
	 * Fetches schemata of all models promoted by backend service.
	 *
	 * The returned promise gets cached on client side.
	 *
	 * @param {boolean} forceUpdate set true to force reading schema from backend service
	 * @returns {Promise<object>} promises schemata of all promoted models
	 */
	static schemata( forceUpdate = false ) {
		if ( forceUpdate || !$$schemata ) {
			$$schemata = this.query( "GET", ".schema" )
				.then( schemata => {
					this.$$cachedSchemata = schemata;

					return schemata;
				} );
		}

		return $$schemata;
	}

	/**
	 * Fetches schema of current model.
	 *
	 * The returned promise gets cached on client side.
	 *
	 * @returns {Promise<object>} promises current model's schema
	 */
	static get schema() {
		return this.schemata().then( schemata => schemata[this.slug] || {} );
	}

	/**
	 * Lists items of current model.
	 *
	 * @param {int} offset number of items to skip
	 * @param {int} limit maximum number of items to fetch
	 * @param {string} sortBy name of property to sort results by
	 * @param {boolean} sortAscendingly set false for fetching items sorted in reverse order
	 * @returns {Promise<{items:object[], count:int}>} promises items of current model as well as total count of items available
	 */
	static list( { offset = 0, limit = Infinity, sortBy = null, sortAscendingly = true } = {} ) {
		return this.find( null, { offset, limit, sortBy, sortAscendingly } );
	}

	/**
	 * Lists items of current model with values of selected property matching
	 * provided one.
	 *
	 * @param {string} property name of property to test
	 * @param {*} value accepted value of selected property
	 * @param {int} offset number of items to skip
	 * @param {int} limit maximum number of items to fetch
	 * @param {string} sortBy name of property to sort results by
	 * @param {boolean} sortAscendingly set false for fetching items sorted in reverse order
	 * @returns {Promise<{items:object[], count:int}>} promises items of current model as well as total count of items available
	 */
	static findByAttribute( property, value, { offset = 0, limit = Infinity, sortBy = null, sortAscendingly = true } = {} ) {
		return this.find( { eq: { name: property, value } }, { offset, limit, sortBy, sortAscendingly } );
	}

	/**
	 * Lists items of current model matching provided filter.
	 *
	 * @param {Filter} filter description of filter to apply for picking desired items
	 * @param {int} offset number of items to skip
	 * @param {int} limit maximum number of items to fetch
	 * @param {string} sortBy name of property to sort results by
	 * @param {boolean} sortAscendingly set false for fetching items sorted in reverse order
	 * @returns {Promise<{items:object[], count:int}>} promises items of current model as well as total count of items available
	 */
	static find( filter, { offset = 0, limit = Infinity, sortBy = null, sortAscendingly = true } = {} ) {
		const query = {};

		if ( filter ) {
			query.q = this._compileFilter( filter );
		}

		if ( offset > 0 ) {
			query.offset = parseInt( offset );
		}

		if ( limit < Infinity && limit > 0 ) {
			query.limit = parseInt( limit );
		}

		if ( sortBy ) {
			return this.schema
				.then( schema => {
					if ( schema.props.hasOwnProperty( sortBy ) || schema.computed.hasOwnProperty( sortBy ) ) {
						query.sortBy = sortBy;

						if ( !sortAscendingly ) {
							query.descending = 1;
						}

						return this.query( "GET", this.slug, null, query, { "x-count": 1 } );
					}

					throw new TypeError( "invalid property for sorting" );
				} );
		}

		return this.query( "GET", this.slug, null, query, { "x-count": 1 } )
			.then( ( { items, count } ) => {
				const numItems = items.length;
				const qualified = new Array( numItems );

				for ( let i = 0; i < numItems; i++ ) {
					const item = items[i];

					const wrapped = qualified[i] = new this( item.uuid, item );
					wrapped.$loaded = Promise.resolve( wrapped );
				}

				return { items: qualified, count };
			} );
	}

	/**
	 * Serializes provided filter for transmitting to backend service as part of
	 * a REST request.
	 *
	 * @param {Filter|string} filter description of filter, string if filter has been compiled before
	 * @returns {string} compiled filter suitable for passing in REST API
	 * @protected
	 */
	static _compileFilter( filter ) {
		if ( typeof filter === "string" ) {
			return filter;
		}

		if ( filter && typeof filter === "object" ) {
			const keys = Object.keys( filter );
			if ( keys.length !== 1 ) {
				throw new TypeError( "invalid number of filter operations" );
			}

			const key = keys[0];
			const operation = key.toLowerCase();
			const { name, value, lower, upper } = filter[key] || {};

			if ( name == null || name.trim() === "" || name.indexOf( ":" ) > -1 ) {
				throw new TypeError( "missing or invalid name of property to filter" );
			}

			switch ( operation ) {
				case "null" :
				case "notnull" :
					return `${name}:${operation}`;

				case "eq" :
				case "noteq" :
				case "neq" :
				case "lt" :
				case "lte" :
				case "gt" :
				case "gte" :
					return `${name}:${operation.replace( /^noteq$/, "neq" )}:${value}`;

				case "between" :
					return `${name}:${operation}:${lower}:${upper}`;
			}
		}

		throw new TypeError( "invalid filter to be compiled" );
	}

	/**
	 * Fetches item selected by its UUID.
	 *
	 * @param {string} uuid UUID of item to fetch from backend service
	 * @returns {Promise<Model>} promises fetched item
	 */
	static select( uuid ) {
		return Promise.all( [
			this.schema,
			this.query( "GET", [ this.slug, uuid ] ),
		] )
			.then( ( [ _, record ] ) => { // eslint-disable-line no-unused-vars
				const item = new this( record.uuid, record );

				item.$loaded = Promise.resolve( item );

				return item;
			} );
	}

	/**
	 * Fetches options cache of current model.
	 *
	 * An options cache is used for re-using maps of current model's items' UUIDs
	 * into string describing either item in association with list boxes or
	 * similar in GUIs for picking instances of model as related to some other
	 * model's items.
	 *
	 * @returns {OptionsListCache} current model's local cache of options lists
	 */
	static get optionsCache() {
		if ( !this.$$optionsCache ) {
			Object.defineProperty( this, "$$optionsCache", { value: new OptionsListCache( this ) } );
		}

		return this.$$optionsCache;
	}

	/**
	 * Loads properties of current item from backend service.
	 *
	 * @returns {Promise<Model>} promises current item (fluent interface) with properties loaded
	 */
	load() {
		if ( !this.$loaded ) {
			if ( !this.uuid ) {
				return Promise.reject( new TypeError( "can't load freshly created item" ) );
			}

			if ( this.$changed.length > 0 ) {
				return Promise.reject( new TypeError( "must not load record after adjusting properties" ) );
			}

			const slug = this.constructor.slug;

			this.$loaded = this.constructor.query( "GET", [ slug, this.uuid ] )
				.then( record => {
					const props = this.constructor.$$cachedSchemata[slug].props;
					const names = Object.keys( props );
					const numNames = names.length;

					for ( let i = 0; i < numNames; i++ ) {
						const name = names[i];
						const { type } = props[name];
						const raw = record[name];
						const isBuffer = typeof raw === "object" && raw && raw.type === "Buffer" && Array.isArray( raw.data );
						const value = isBuffer ? new Uint8ClampedArray( raw.data ) : raw;

						switch ( String( type || "string" ).trim().toLowerCase() ) {
							case "uuid" :
								if ( isBuffer && value.length === 16 ) {
									let hex = "";

									for ( let j = 0; j < 16; j++ ) {
										hex += ( "0" + value[j].toString( 16 ) ).slice( -2 );
									}

									this[name] = hex.replace( /^(.{8})(.{4})(.{4})(.{4})(.{12})$/, "$1-$2-$3-$4-$5" );
								} else {
									this[name] = String( value ).trim() || null;
								}
								break;

							default :
								this[name] = isBuffer ? new Blob( [value], { type: "application/octet-stream" } ) : value;
						}
					}

					this.constructor.emit( "fetched", this );

					return this;
				} );
		}

		return this.$loaded;
	}

	/**
	 * Sends changed properties of item to backend service.
	 *
	 * @returns {Promise<Model>} promises saved item (fluent interface)
	 */
	save() {
		const changed = this.$changed;
		const numChanged = changed.length;

		if ( numChanged < 1 ) {
			return Promise.resolve( this );
		}

		if ( !this.$valid ) {
			return Promise.reject( new Error( "saving invalid properties rejected" ) );
		}

		const diff = {};
		for ( let i = 0; i < numChanged; i++ ) {
			const name = changed[i];

			diff[name] = this[name];
		}

		let query;

		if ( this.uuid ) {
			query = this.constructor.query( "PATCH", [ this.constructor.slug, this.uuid ], diff );
		} else {
			query = this.constructor.query( "POST", [this.constructor.slug], diff );
		}

		return query
			.then( response => {
				if ( !this.uuid ) {
					if ( !response.uuid ) {
						throw new TypeError( "missing UUID of created item in response from backend service" );
					}

					Object.defineProperty( this, "uuid", {
						value: this.constructor.normalizeUuid( response.uuid ),
						enumerable: true,
					} );
				}

				this.constructor.emit( "updated", this );
				this.constructor.optionsCache.dropAll();

				return this;
			} );
	}

	/**
	 * Removes item at backend service.
	 *
	 * @returns {Promise<Model>} promises item removed (fluent interface)
	 */
	remove() {
		if ( !this.uuid ) {
			return Promise.reject( new TypeError( "item has not been created before" ) );
		}

		return this.constructor.query( "DELETE", [ this.constructor.slug, this.uuid ] )
			.then( () => {
				this.constructor.emit( "removed", this );

				return this;
			} );
	}
}
