/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

/**
 * @typedef {object} OptionRecord
 * @property {string} uuid UUID of represented item
 * @property {string} label textual representation of item
 */

/**
 * Implements local cache for options list of selected model.
 */
export class OptionsListCache {
	/**
	 * @param {class<Model>} modelClass implementation of model this cache is for
	 */
	constructor( modelClass ) {
		Object.defineProperties( this, {
			/**
			 * Refers to implementation of model all listed options are addressing.
			 *
			 * @name OptionsListCache#Model
			 * @property {class<Model>}
			 * @readonly
			 */
			Model: { value: modelClass },

			/**
			 * Caches lists of options each addressing items of associated model.
			 *
			 * @name OptionsListCache#$$options
			 * @property {Map<string, OptionRecord[]>}
			 * @protected
			 * @readonly
			 */
			$$options: { value: new Map() },
		} );
	}

	/**
	 * Converts provided data into serializable data assuring that same input
	 * data results in same result on every invocation.
	 *
	 * @param {*} object data to convert
	 * @returns {*} converted data
	 */
	static serializable( object ) {
		switch ( typeof object ) {
			case "object" : {
				if ( !object ) {
					return null;
				}

				let list, write = 0;

				if ( Array.isArray( object ) ) {
					const numItems = object.length;
					list = new Array( numItems );

					for ( let read = 0; read < numItems; read++ ) {
						const value = this.serializable( object[read] );

						if ( value != null ) {
							list[write++] = value;
						}
					}
				} else {
					const names = Object.keys( object );
					names.sort( ( l, r ) => l.localeCompare( r ) );

					const numItems = names.length;
					list = new Array( numItems * 2 );

					for ( let read = 0; read < numItems; read++ ) {
						const name = names[read];
						const value = this.serializable( object[name] );

						if ( value != null ) {
							list[write++] = name;
							list[write++] = value;
						}
					}
				}

				list.splice( write );

				return list;
			}

			case "function" :
			case "undefined" :
				return null;

			case "number" :
			case "boolean" :
				return object;

			default :
				return String( object );
		}
	}

	/**
	 * Calculates selector of cached options list according to provided
	 * criteria.
	 *
	 * @param {string} labelTemplate template used for generating labels representing either item in list
	 * @param {*} filter optional description of filter used to control items included with options list
	 * @returns {string} selector addressing cached options list
	 */
	static computeSelector( labelTemplate, filter ) {
		const serializable = this.serializable( filter );

		return labelTemplate + "$$" + ( serializable == null ? "" : JSON.stringify( serializable ) );
	}

	/**
	 * Drops all previously cached options lists.
	 *
	 * @returns {OptionsListCache} fluent interface
	 */
	dropAll() {
		this.$$options.clear();

		return this;
	}

	/**
	 * Drops options lists generated with provided label template.
	 *
	 * @param {string} labelTemplate template used to create label representing either item
	 * @param {object} filter description of filter used to control set of items to be covered in options list
	 * @returns {OptionsListCache} fluent interface
	 */
	drop( labelTemplate, filter = null ) {
		if ( filter == null ) {
			for ( const key of this.$$options.keys() ) {
				if ( String( key ).startsWith( labelTemplate + "$$" ) ) {
					this.$$options.delete( key );
				}
			}
		} else {
			this.$$options.delete( this.constructor.computeSelector( labelTemplate, filter ) );
		}

		return this;
	}

	/**
	 * Detects whether there is a previously fetched options list for provided
	 * criteria.
	 *
	 * @param {string} labelTemplate template used to create label representing either item
	 * @param {*} filter description of filter used to control set of items to be covered in options list
	 * @returns {boolean} true if there is a matching options list in cache
	 */
	has( labelTemplate, filter = null ) {
		return Array.isArray( this.$$options.get( this.constructor.computeSelector( labelTemplate, filter ) ) );
	}

	/**
	 * Detects whether there is a previously fetched options list for provided
	 * criteria.
	 *
	 * @param {string} labelTemplate template used to create label representing either item
	 * @param {*} filter description of filter used to control set of items to be covered in options list
	 * @returns {?Array<OptionRecord>} cached options list, nullish if missing options list on cache
	 */
	get( labelTemplate, filter = null ) {
		const list = this.$$options.get( this.constructor.computeSelector( labelTemplate, filter ) );

		return Array.isArray( list ) ? list : null;
	}

	/**
	 * Fetches options list for provided criteria replacing any existing list in
	 * local cache.
	 *
	 * @param {string} labelTemplate template used to create label representing either item
	 * @param {*} filter description of filter used to control set of items to be covered in options list
	 * @param {boolean} force set true to enforce fetching list from remote service again
	 * @returns {Promise<Array<OptionRecord>>} promised options list
	 */
	fetch( labelTemplate, filter = null, force = false ) {
		const selector = this.constructor.computeSelector( labelTemplate, filter );
		let tracked = this.$$options.get( selector );

		if ( !( tracked instanceof Promise ) && ( force || !Array.isArray( tracked ) ) ) {
			tracked = ( filter ? this.Model.find( filter ) : this.Model.list() )
				.then( ( { items } ) => {
					const numItems = items.length;

					for ( let i = 0; i < numItems; i++ ) {
						const item = items[i];

						items[i] = {
							uuid: item.uuid,
							label: labelTemplate.replace( /%%([^%]+)%%/g, ( _, name ) => item[name] ),
						};
					}

					this.$$options.set( selector, items );

					return items;
				} );

			this.$$options.set( selector, tracked );
		}

		return tracked instanceof Promise ? tracked : Promise.resolve( tracked );
	}
}
